﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newGameManagerData", menuName = "GameManager/GameData")]
public class GameData : ScriptableObject
{
    public GameManager.GameState CurrentGameState;
}
