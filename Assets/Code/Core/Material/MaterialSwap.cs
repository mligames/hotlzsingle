﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MaterialSwap : ScriptableObject
{
    public abstract void Swap(UnityEngine.GameObject go, Material mat);
}
