﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Timer))]
public class TimerEditor : Editor
{
    SerializedProperty timeCountProp;
    SerializedProperty currentTimeProp;
    SerializedProperty finishedProp;

    void OnEnable()
    {
        // Fetch the objects from the GameObject script to display in the inspector
        timeCountProp = serializedObject.FindProperty("timeCount");
        currentTimeProp = serializedObject.FindProperty("currentTime");
        finishedProp = serializedObject.FindProperty("finished");
    }
    public override void OnInspectorGUI()
    {
        //The variables and GameObject from the MyGameObject script are displayed in the Inspector with appropriate labels
        EditorGUILayout.PropertyField(timeCountProp, new GUIContent("Time Count"), GUILayout.Height(20));
        EditorGUILayout.PropertyField(currentTimeProp, new GUIContent("Current Time"));
        EditorGUILayout.PropertyField(finishedProp, new GUIContent("Finished"));

        // Apply changes to the serializedProperty - always do this at the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
    }
}
