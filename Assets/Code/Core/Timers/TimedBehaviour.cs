﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.Timers
{
    /// <summary>
    /// Abstract based class for helping with timing in MonoBehaviours
    /// </summary>
    public abstract class TimedBehaviour : MonoBehaviour
    {
        [Header("Time in seconds to finish")]
        [SerializeField] private float finishTime;
        [SerializeField] private float currentTime;
        public bool timerFinished;

        /// <summary>
        /// Normalized progress of the timer
        /// </summary>
        public float NormalizedProgress
        {
            get { return Mathf.Clamp(currentTime / finishTime, 0f, 1f); }
        }

        protected virtual void Start()
        {
            SetTime(finishTime);
            currentTime = 0f;
        }

        /// <summary>
        /// Returns the result of AssessTime
        /// </summary>
        /// <param name="deltaTime">change in time between ticks</param>
        /// <returns>true if the timer has elapsed, false otherwise</returns>
        public virtual bool Tick(float deltaTime)
        {
            return AssessTime(deltaTime);
        }

        /// <summary>
        /// Checks if the time has elapsed and fires the tick event
        /// </summary>
        /// <param name="deltaTime">the change in time between assessments</param>
        /// <returns>true if the timer has elapsed, false otherwise</returns>
        protected bool AssessTime(float deltaTime)
        {
            currentTime += deltaTime;
            if (currentTime >= finishTime)
            {
                timerFinished = true;
                return true;
            }
            timerFinished = false;
            return false;
        }

        /// <summary>
        /// Resets the current time to 0
        /// </summary>
        public void ResetTimer()
        {
            currentTime = 0;
            timerFinished = false;
        }

        /// <summary>
        /// Sets the elapsed time
        /// </summary>
        /// <param name="newTime">sets the time to a new value</param>
        public void SetTime(float newTime)
        {
            finishTime = newTime;

            if (newTime <= 0)
            {
                finishTime = 0.1f;
            }
        }
    }
}
