﻿using Core.Timers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour
{
    public Text timerText;
    public TimedBehaviour timedBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        if (timedBehaviour == null)
            timedBehaviour = GetComponent<TimedBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timedBehaviour.timerFinished)
            timerText.text = "Ready " + timedBehaviour.NormalizedProgress.ToString("F2");
        else
            timerText.text = "Not Ready " + timedBehaviour.NormalizedProgress.ToString("F2");
    }
}
