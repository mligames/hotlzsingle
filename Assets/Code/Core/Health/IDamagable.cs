﻿using Core.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable : ISerializableInterface
{
    float MaxHealth { get; set; }
    float CurrentHealth { get; set; }
    void ApplyDamage(float damageAmount);
    void Die();
}
[Serializable]
public class IDamageableComponent : SerializableInterface<IDamagable>
{
}