﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledParticleEffect : ParticleEffect
{
    public ParticleEffectPool ParticleEffectPool;
    public override void Play()
    {
        base.Play();
        ReturnToPool();
    }
    public void ReturnToPool()
    {
        ParticleEffectPool.ReturnToPool(this);
    }
}
