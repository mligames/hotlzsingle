﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect : MonoBehaviour
{
    [Header("Inspector Set")]
    public UnityEngine.GameObject particleEffect;
    public float playDuration;
    public bool isPlaying;
    // Start is called before the first frame update

    public virtual void Play()
    {
        StopCoroutine(PlayEffect());
        StartCoroutine(PlayEffect());
    }
    IEnumerator PlayEffect()
    {
        isPlaying = true;
        particleEffect.gameObject.SetActive(true);
        yield return new WaitForSeconds(playDuration);
        particleEffect.gameObject.SetActive(false);
        isPlaying = false;
    }
}
