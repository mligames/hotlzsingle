﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newParticleEffect", menuName = "Particle Effects/Particle Effect")]
public class ParticleEffectData : ScriptableObject
{
    public UnityEngine.GameObject ParticleEffectPrefab;
}
