﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffectPool : ParticleEffect
{
    public PooledParticleEffect ParticleObject;
    public int PoolCount;
    [SerializeField] private List<PooledParticleEffect> ParticlePoolList;

    private void Awake()
    {
        ParticlePoolList = new List<PooledParticleEffect>();
    }
    private void Start()
    {
        FillParticlePool();
    }

    public void FillParticlePool()
    {
        for (int i = 0; i < PoolCount; i++)
            CreateInstance();
    }
    public PooledParticleEffect CreateInstance()
    {
        PooledParticleEffect particle = Instantiate(ParticleObject,transform);
        particle.ParticleEffectPool = this;
        ParticlePoolList.Add(particle);
        return particle;
    }
    public PooledParticleEffect FetchFromPool()
    {
        for (int i = 0; i < ParticlePoolList.Count; i++)
        {
            if (!ParticlePoolList[i].isPlaying)
            {
                ParticlePoolList[i].transform.parent = null;
                return ParticlePoolList[i];
            }
        }
        return CreateInstance();
    }
    public void ReturnToPool(PooledParticleEffect pooledEffect)
    {
        pooledEffect.transform.parent = transform;
    }
}
