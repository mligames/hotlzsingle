﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParticleEffect : MonoBehaviour
{
    public float playDuration;
    // Start is called before the first frame update
    void Start()
    {
        transform.parent = null;
        Destroy(gameObject, playDuration);
    }
}
