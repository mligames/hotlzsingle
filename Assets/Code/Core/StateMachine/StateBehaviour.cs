﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateBehaviour : ScriptableObject
{
    public abstract void Enter(StateMachineController controller);

    public abstract void StateUpdate(StateMachineController controller);

    public abstract void Exit(StateMachineController controller);
}
