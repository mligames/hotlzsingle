﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineController : MonoBehaviour
{
    public StateBehaviour CurrentState;
    [SerializeField] private bool inTransition;
    public void ChangeState(StateBehaviour stateBehaviour)
    {
        CurrentState = stateBehaviour;
    }

    public void Transition(StateBehaviour value)
    {
        if (CurrentState == value || inTransition)
            return;

        inTransition = true;

        if (CurrentState != null)
            CurrentState.Exit(this);

        CurrentState = value;

        if (CurrentState != null)
            CurrentState.Enter(this);

        inTransition = false;
    }
}
