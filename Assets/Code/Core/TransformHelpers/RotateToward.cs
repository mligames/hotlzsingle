﻿using GameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToward : MonoBehaviour
{
    public Transform target;
    public float speed;
    public RotateAxis rotateAxisType;

    public Vector3 rotationAxis;
    // Start is called before the first frame update
    void Start()
    {
        switch (rotateAxisType)
        {
            case RotateAxis.X:
                rotationAxis = transform.right;
                break;
            case RotateAxis.Y:
                rotationAxis = transform.up;
                break;
            case RotateAxis.Z:
                rotationAxis = transform.forward;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotateTransform();
    }
    public void RotateTransform()
    {
        if (target == null)
            return;

        Vector3 targetDir = target.position - transform.position;
        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        //newDir.x = 0;
        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);
    }

}
