﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageEventArgs : EventArgs
{
    public readonly string message;
    public MessageEventArgs(string m)
    {
        message = m;
    }
}
