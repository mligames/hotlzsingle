﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleObserver : MonoBehaviour
{
    public const string Notification = Notifications.TEST_NOTIFICATION;
    void OnEnable()
    {
        this.AddObserver(OnNotification, Notification);
    }
    void OnDisable()
    {
        this.RemoveObserver(OnNotification, Notification);
    }

    void OnNotification(object arg1, object arg2)
    {
        Debug.Log("Got it! " + ((MessageEventArgs)arg2).message);
    }

}
