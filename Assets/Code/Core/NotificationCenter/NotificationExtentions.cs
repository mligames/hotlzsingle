﻿using UnityEngine;
using System;
using System.Collections;
using Handler = System.Action<System.Object, System.Object>;

public static class NotificationExtensions
{
    public static void PostNotification(this object sender, string notificationName)
    {
        NotificationCenter.instance.PostNotification(notificationName, sender);
    }

    public static void PostNotification(this object sender, string notificationName, object e)
    {
        NotificationCenter.instance.PostNotification(notificationName, sender, e);
    }

    public static void AddObserver(this object observer, Handler handler, string notificationName)
    {
        NotificationCenter.instance.AddObserver(handler, notificationName);
    }

    public static void AddObserver(this object observer, Handler handler, string notificationName, object sender)
    {
        NotificationCenter.instance.AddObserver(handler, notificationName, sender);
    }

    public static void RemoveObserver(this object observer, Handler handler, string notificationName)
    {
        NotificationCenter.instance.RemoveObserver(handler, notificationName);
    }

    public static void RemoveObserver(this object observer, Handler handler, string notificationName, System.Object sender)
    {
        NotificationCenter.instance.RemoveObserver(handler, notificationName, sender);
    }
}