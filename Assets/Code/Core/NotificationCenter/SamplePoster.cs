﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamplePoster : MonoBehaviour
{
    public const string Notification = Notifications.TEST_NOTIFICATION;
    public int listenerCount = 250;
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
            Test();
    }
    void Test()
    {
        Listener[] listeners = new Listener[listenerCount];
        for (int i = 0; i < listenerCount; ++i)
        {
            listeners[i] = new Listener();
            listeners[i].Enable();
        }

        this.PostNotification(Notification, new MessageEventArgs("Hello World!"));
        Debug.Log("Listener Count " + listenerCount);
        for (int i = 0; i < listeners.Length; ++i)
            listeners[i].Disable();
        //NotificationCenter.instance.Clean();
    }
}
public class Listener
{
    public const string TestNotification = Notifications.TEST_NOTIFICATION;
    public const string Clear = "Listener.Clear";
    public void Enable()
    {
        this.AddObserver(OnTest, TestNotification);
        this.AddObserver(OnClear, Listener.Clear);
    }

    public void Disable()
    {
        this.RemoveObserver(OnTest, TestNotification);
        this.RemoveObserver(OnClear, Listener.Clear);
    }

    void OnTest(object sender, object info)
    {
        this.PostNotification(Listener.Clear);
    }

    void OnClear(object sender, object info)
    {
        this.RemoveObserver(OnTest, TestNotification);
    }
}