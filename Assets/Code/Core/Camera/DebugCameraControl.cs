﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCameraControl : MonoBehaviour
{
    public KeyCode targetRayKey;
    public KeyCode unitRayKey;

    public float lookSpeedH = 2f;
    public float lookSpeedV = 2f;
    public float zoomSpeed = 2f;
    public float dragSpeed = 5f;

    private float yaw;
    private float pitch;

    public Transform raycastPoint;
    public float raycastRange;
    public LayerMask defaultLayerMask;
    public LayerMask unitLayerMask;
    public LayerMask targetLayerMask;
    
    private void Start()
    {
        // x - right    pitch
        // y - up       yaw
        // z - forward  roll
        yaw = transform.eulerAngles.y;
        pitch = transform.eulerAngles.x;
    }

    void Update()
    {
        if (!enabled) return;


        if (Input.GetMouseButton(0))
        {
            Ray ray = new Ray
            {
                origin = raycastPoint.position,
                direction = raycastPoint.forward,
            };

            if (Physics.Raycast(ray, out RaycastHit raycastHit, raycastRange, defaultLayerMask))
            {
                Debug.Log("Debug Raycast: " + raycastHit.collider.gameObject.name + " " + raycastHit.collider.transform.root.name);
            }
        }
        if (Input.GetKeyUp(unitRayKey))
        {
            Ray ray = new Ray
            {
                origin = raycastPoint.position,
                direction = raycastPoint.forward,
            };

            if (Physics.Raycast(ray, out RaycastHit raycastHit, raycastRange, unitLayerMask))
            {
                Debug.Log("Debug Raycast: " + raycastHit.collider.transform.root.gameObject.name + " " + raycastHit.collider.name);
            }
        }
        if (Input.GetKeyUp(targetRayKey))
        {
            Ray ray = new Ray
            {
                origin = raycastPoint.position,
                direction = raycastPoint.forward,
            };

            if (Physics.Raycast(ray, out RaycastHit raycastHit, raycastRange, targetLayerMask))
            {
                DamageZone damageZone = raycastHit.collider.GetComponent<DamageZone>();
                if(damageZone != null)
                {
                    damageZone.ApplyDamage(50f);
                    Debug.Log("Debug Target Raycast: " + raycastHit.collider.transform.root.gameObject.name);
                }           
            }
            Debug.Log("Debug Target Raycast: Hit Nothing" );
        }
        //Look around with Right Mouse
        if (Input.GetMouseButton(1))
        {
            yaw += lookSpeedH * Input.GetAxis("Mouse X");
            pitch -= lookSpeedV * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0f);

            Vector3 offset = Vector3.zero;
            float offsetDelta = Time.deltaTime * dragSpeed;
            if (Input.GetKey(KeyCode.LeftShift)) offsetDelta *= 5.0f;
            if (Input.GetKey(KeyCode.S)) offset.z -= offsetDelta;
            if (Input.GetKey(KeyCode.W)) offset.z += offsetDelta;
            if (Input.GetKey(KeyCode.A)) offset.x -= offsetDelta;
            if (Input.GetKey(KeyCode.D)) offset.x += offsetDelta;
            if (Input.GetKey(KeyCode.Q)) offset.y -= offsetDelta;
            if (Input.GetKey(KeyCode.E)) offset.y += offsetDelta;

            transform.Translate(offset, Space.Self);
        }

        //drag camera around with Middle Mouse
        if (Input.GetMouseButton(2))
        {
            transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0);
        }

        //Zoom in and out with Mouse Wheel
        transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);

    }
}
