﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public CameraSetting cameraSetting;

    private void Start()
    {
        //target = GetComponentInParent<Transform>().transform;
        transform.parent = null;
        transform.Rotate(cameraSetting.tiltAngle);
    }
    private void LateUpdate()
    {
        transform.position = target.position + cameraSetting.offset;
    }
}
