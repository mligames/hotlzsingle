﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileFactory : MonoBehaviour
{
    public abstract ProjectileData ProjectileData { get; }
    public int ProjectilePoolCount;
    [SerializeField] private List<UnityEngine.GameObject> ProjectilePoolList;
    public Collider[] IgnoreColliders;
    protected virtual void Awake()
    {
        ProjectilePoolList = new List<UnityEngine.GameObject>();
    }
    protected virtual void Start()
    {
        FillProjectilePool();
    }
    public abstract GameObject CreateProjectile();

    public void FillProjectilePool()
    {
        for (int i = 0; i < ProjectilePoolCount; i++)
        {
            ProjectilePoolList.Add(CreateProjectile());
        }
    }
    public GameObject FetchFromPool()
    {
        for (int i = 0; i < ProjectilePoolList.Count; i++)
        {
            if (!ProjectilePoolList[i].activeSelf)
            {
                ProjectilePoolList[i].GetComponent<Projectile>().SelfDestructTimer.ResetTimer();
                ProjectilePoolList[i].transform.parent = null;
                return ProjectilePoolList[i];
            }
        }
        return null;
    }
    public void ReturnToPool(Projectile projectileArg)
    {
        projectileArg.gameObject.SetActive(false);
        projectileArg.transform.parent = transform;
    }
}
