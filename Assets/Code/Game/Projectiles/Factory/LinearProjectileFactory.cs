﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearProjectileFactory : ProjectileFactory
{
    [SerializeField] private LinearProjectileData linearProjectileData;
    public override ProjectileData ProjectileData => linearProjectileData;

    public override GameObject CreateProjectile()
    {
        UnityEngine.GameObject projectile = Instantiate(linearProjectileData.ProjectilePrefab, transform);
        projectile.SetActive(false);
        projectile.GetComponent<LinearProjectile>().ProjectileFactory = this;
        projectile.GetComponent<LinearProjectile>().IgnoreColliders = IgnoreColliders;
        projectile.GetComponent<LinearProjectile>().Damage = linearProjectileData.Damage;
        projectile.GetComponent<LinearProjectile>().Speed = linearProjectileData.Speed;
        projectile.GetComponent<LinearProjectile>().ImpactExplosionRadius = linearProjectileData.ImpactExplosionRadius;
        for (int i = 0; i < IgnoreColliders.Length; i++)
        {
            Physics.IgnoreCollision(IgnoreColliders[i], projectile.GetComponent<CapsuleCollider>());
        }

        return projectile;
    }

}
