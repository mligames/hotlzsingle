﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingProjectileFactory : ProjectileFactory
{
    [SerializeField] private HomingProjectileData homingProjectileData;
    public override ProjectileData ProjectileData => homingProjectileData;

    public override GameObject CreateProjectile()
    {
        UnityEngine.GameObject projectile = Instantiate(homingProjectileData.ProjectilePrefab, transform);
        projectile.SetActive(false);
        projectile.GetComponent<HomingProjectile>().ProjectileFactory = this;
        projectile.GetComponent<HomingProjectile>().IgnoreColliders = IgnoreColliders;
        projectile.GetComponent<HomingProjectile>().Damage = homingProjectileData.Damage;
        projectile.GetComponent<HomingProjectile>().Speed = homingProjectileData.Speed;
        projectile.GetComponent<HomingProjectile>().ImpactExplosionRadius = homingProjectileData.ImpactExplosionRadius;
        for (int i = 0; i < IgnoreColliders.Length; i++)
        {
            Physics.IgnoreCollision(IgnoreColliders[i], projectile.GetComponent<CapsuleCollider>());
        }

        return projectile;
    }
}
