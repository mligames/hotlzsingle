﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearProjectile : Projectile
{
    [SerializeField] private LinearProjectileData linearProjectileData;
    public override ProjectileData ProjectileData { get => linearProjectileData; set => linearProjectileData = (LinearProjectileData)value; }
    public UnityEngine.GameObject ExplosionFXPrefab;
    public float ImpactExplosionRadius;
    public LayerMask hitLayerMask;
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
        MoveProjectile();
    }

    public override void Detonate()
    {
        if (!Detonated)
        {
            UnityEngine.GameObject particle = Instantiate(ExplosionFXPrefab, transform);
            particle.transform.position = transform.position;
            particle.transform.parent = null;
            Destroy(particle, 1f);
        }

        Detonated = true;

        AreaDamageEffect();

        gameObject.SetActive(false);
        transform.parent = ProjectileFactory.transform;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        Detonate();
    }
    public override void MoveProjectile()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * Speed);
    }
    public void AreaDamageEffect()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, ImpactExplosionRadius, hitLayerMask);
        if (colliders.Length > 0)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                // May need a little logic to handle multiple collider hits on one unit.
                HealthComponent hitHealthComponent = colliders[i].transform.root.GetComponent<HealthComponent>();
                if (hitHealthComponent != null)
                {
                    //Debug.Log(hitHealthComponent.gameObject.name + " was hit with an explosion!");
                    hitHealthComponent.Damage(Damage);
                }
            }
        }
    }
}
