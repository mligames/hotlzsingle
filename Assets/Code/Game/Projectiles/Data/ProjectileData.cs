﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileData : ScriptableObject
{
    public string ProjectileName;
    public UnityEngine.GameObject ProjectilePrefab;
    public float Damage;
    public float Speed;
    public float SelfDestructTime;
    public float ImpactExplosionRadius;
    public abstract GameObject CreateProjectile();
}
