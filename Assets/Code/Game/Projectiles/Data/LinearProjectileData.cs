﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newLinearProjectileData", menuName = "Projectiles/Linear Projectile")]
public class LinearProjectileData : ProjectileData
{
    public override GameObject CreateProjectile()
    {
        UnityEngine.GameObject projectile = Instantiate(ProjectilePrefab);
        projectile.SetActive(false);
        projectile.name = ProjectileName;
        projectile.GetComponent<LinearProjectile>().Speed = Speed;
        projectile.GetComponent<LinearProjectile>().Damage = Damage;
        projectile.GetComponent<LinearProjectile>().SelfDestructTimerCount = SelfDestructTime;
        projectile.GetComponent<LinearProjectile>().ImpactExplosionRadius = ImpactExplosionRadius;
        return projectile;
    }


}
