﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newHomingProjectileData", menuName = "Projectiles/Homing Projectile")]
public class HomingProjectileData : ProjectileData
{
    public override GameObject CreateProjectile()
    {
        UnityEngine.GameObject projectile = Instantiate(ProjectilePrefab);
        projectile.SetActive(false);
        projectile.name = ProjectileName;
        projectile.GetComponent<HomingProjectile>().Speed = Speed;
        projectile.GetComponent<HomingProjectile>().Damage = Damage;
        projectile.GetComponent<HomingProjectile>().SelfDestructTimerCount = SelfDestructTime;
        projectile.GetComponent<HomingProjectile>().ImpactExplosionRadius = ImpactExplosionRadius;
        return projectile;
    }
}
