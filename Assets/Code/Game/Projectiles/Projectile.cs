﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    public abstract ProjectileData ProjectileData { get; set; }
    public ProjectileFactory ProjectileFactory;
    public float SelfDestructTimerCount;
    public Timer SelfDestructTimer;
    public float Speed;
    public float Damage;
    public Collider[] IgnoreColliders;
    public bool Detonated;

    protected virtual void Awake()
    {
        SelfDestructTimer = new Timer(SelfDestructTimerCount);

    }

    // Start is called before the first frame update
    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        SelfDestructTimer.Tick(Time.deltaTime);
        if (SelfDestructTimer.finished)
            Detonate();

    }
    protected virtual void OnDisable()
    {
        Detonated = false;
    }
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger)
        {
            //Debug.Log(name + " Hit " + other.name);
            Detonate();
        }
    }

    public abstract void MoveProjectile();
    public abstract void Detonate();

}
