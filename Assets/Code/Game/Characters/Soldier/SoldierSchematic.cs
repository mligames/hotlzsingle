﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newSoldierSchematic",menuName ="Schematics/Units/Soldier")]
public class SoldierSchematic : ScriptableObject
{
    public FactionData FactionData;
    public UnityEngine.GameObject SoldierPrefab;
    public Material UniformMaterial;
    public void ChangeUniform(UnityEngine.GameObject soldier)
    {
        soldier.transform.Find("Model").transform.Find("Head").GetComponent<SkinnedMeshRenderer>().material = UniformMaterial;
        soldier.transform.Find("Model").transform.Find("Body").GetComponent<SkinnedMeshRenderer>().material = UniformMaterial;
    }

    public GameObject CreateUnit()
    {
        UnityEngine.GameObject soldierObject = Instantiate(SoldierPrefab);
        soldierObject.SetActive(false);

        soldierObject.GetComponent<TargetScanner>().FactionData = FactionData;
        // Change Uniform
        ChangeUniform(soldierObject);
        soldierObject.name = "Soldier (" + FactionData.FactionName + ")";
        return soldierObject;
    }
}
