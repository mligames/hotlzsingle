﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(HealthComponent))]
public class SoldierController : Target
{
    public StateMachineController StateMachineController;
    public SoldierIdleState SoldierIdleState;
    public SoldierAttackState SoldierAttackState;
    public SoldierPatrolState SoldierPatrolState;
    //public Animator Animator;
    public Navigator Navigator;
    public HealthComponent HealthComponent;
    public TargetScanner TargetScanner;
    public AttackBehaviour AttackBehaviour;
    public Transform DebugPoint;
    public bool Pooled { get; set; }
    public bool Alive { get; set; }

    public bool mockHasTarget;

    private void Start()
    {
        SoldierIdleState = ScriptableObject.CreateInstance<SoldierIdleState>();
        SoldierAttackState = ScriptableObject.CreateInstance<SoldierAttackState>();
        SoldierPatrolState = ScriptableObject.CreateInstance<SoldierPatrolState>();
        StateMachineController.Transition(SoldierIdleState);
    }
    // Update is called once per frame
    void Update()
    {
        if(TargetActive)
            StateMachineController.CurrentState.StateUpdate(StateMachineController);
    }

    public void OnDeath()
    {
        StateMachineController.enabled = false;
        Debug.Log(name + " has Died");
        TargetActive = false;
        RemoveTarget();
        gameObject.SetActive(false);
        //StartCoroutine(DeathSequence());
    }
    public void TargetAcquired(Target target)
    {
        Debug.Log(name +"'s Scanner has Acquired Target " + target.TargetPoint.root.gameObject.name);
        //Debug.Log(name + " HandleTargetLost");
        StateMachineController.Transition(SoldierAttackState);
    }
    public void TargetLost()
    {
        Debug.Log(name + "'s Scanner has Lost its Target ");
        StateMachineController.Transition(SoldierIdleState);
    }

    public void AimAtTarget()
    {
        if (TargetScanner.CurrentTarget == null)       
            return;
        
        Vector3 targetDir = TargetScanner.CurrentTarget.transform.position - transform.position;
        targetDir.y = 0;
        // The step size is equal to speed times frame time.
        float step = 280f * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f); 
        //newDir.x = 0;
        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    //protected override void RemoveTarget()
    //{
    //    base.RemoveTarget();
    //}
    IEnumerator DeathSequence()
    {
        yield return new WaitForEndOfFrame();

    }

    public void Repool()
    {
        Pooled = true;
        //TargetActive = false;
        gameObject.SetActive(false);
    }

    #region SetUp
    public void InitComponents()
    {
        //if (Animator == null)
        //    Animator = GetComponentInChildren<Animator>();
        //if (Navigation == null)
        //    Navigation = GetComponent<SoldierNavigation>();
        if (Navigator == null)
            Navigator = GetComponent<Navigator>();
        if (HealthComponent == null)
            HealthComponent = GetComponent<HealthComponent>();

        if (AttackBehaviour == null)
            AttackBehaviour = GetComponent<AttackBehaviour>();

        if (TargetScanner == null)
            TargetScanner = GetComponentInChildren<TargetScanner>();
        TargetScanner.OnTargetAcquired.AddListener(TargetAcquired);
        TargetScanner.OnTargetLost.AddListener(TargetLost);
        TargetActive = true;
    }
    #endregion
}
