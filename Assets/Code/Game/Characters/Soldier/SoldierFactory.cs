﻿using GameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierFactory : MonoBehaviour
{
    public FactoryProductionStatus factoryProductionStatus = FactoryProductionStatus.OFF;
    public SoldierSchematic soldierSchematic;
    public float spawnFrequency;
    public Timer spawnTimer;

    public int maxSpawnCount;
    public int currentSpawnCount;

    public Transform soldierSpawnPoint;
    private void Awake()
    {
        spawnTimer = new Timer(spawnFrequency);
    }
    private void Update()
    {
        switch (factoryProductionStatus)
        {
            case FactoryProductionStatus.ON:
                if (!spawnTimer.finished)
                {
                    spawnTimer.Tick(Time.deltaTime);
                    return;
                }
                else
                {
                    spawnTimer.ResetTimer();
                    SpawnSoldier();
                }
                break;
            case FactoryProductionStatus.OFF:
                if (!spawnTimer.finished)
                {
                    spawnTimer.Tick(Time.deltaTime);
                    return;
                }
                break;
        }
    }
    public void SpawnSoldier()
    {
        UnityEngine.GameObject soldier = soldierSchematic.CreateUnit();
        soldier.transform.position = soldierSpawnPoint.position;
        soldier.gameObject.name = "Soldier (" + soldier.GetComponent<SoldierController>().FactionData.FactionName + ") " + currentSpawnCount;
        soldier.SetActive(true);
        currentSpawnCount++;
        if (currentSpawnCount >= maxSpawnCount)
            factoryProductionStatus = FactoryProductionStatus.OFF;
    }
    public void SpawnOverTime(int numSpawnCount, float waitTime)
    {

    }

}
