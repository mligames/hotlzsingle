﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "State Machine/StateBehaviour/Solider Attack State")]
public class SoldierAttackState : StateBehaviour
{
    public override void Enter(StateMachineController controller)
    {
        Debug.Log("Entered Attack State");
        controller.GetComponent<SoldierController>().Navigator.NavMeshAgent.destination = controller.transform.position;
    }
    public override void StateUpdate(StateMachineController controller)
    {

        if (controller.GetComponent<SoldierController>().TargetScanner.CurrentTarget != null)
        {
            if (controller.GetComponent<SoldierController>().TargetScanner.CurrentTarget.TargetActive)
            {
                controller.GetComponent<SoldierController>().AimAtTarget();
                controller.GetComponent<SoldierController>().AttackBehaviour.TriggerAbility();
            }
        }
        else
        {
            controller.Transition(controller.GetComponent<SoldierController>().SoldierIdleState);
        }

    }
    public override void Exit(StateMachineController controller)
    {
        //Debug.Log("Exited Attack State");
        if(controller.gameObject.activeInHierarchy)
            controller.GetComponent<SoldierController>().Navigator.NavMeshAgent.isStopped = false;
    }
}
