﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "State Machine/StateBehaviour/Solider Patrol State")]
public class SoldierPatrolState : StateBehaviour
{
    public override void Enter(StateMachineController controller)
    {
        //Debug.Log("Entered Patrol State");
    }
    public override void StateUpdate(StateMachineController controller)
    {
        if (controller.GetComponent<SoldierController>().TargetScanner.TargetList.Count > 0)
        {
            controller.GetComponent<SoldierController>().TargetScanner.GetNearestTarget();
            if (controller.GetComponent<SoldierController>().TargetScanner.CurrentTarget != null && controller.GetComponent<SoldierController>().TargetScanner.CurrentTarget.TargetActive)
            {
                controller.Transition(controller.GetComponent<SoldierController>().SoldierAttackState);
                return;
            }
        }

        if (controller.GetComponent<SoldierController>().Navigator.HasArrived())
        {
            controller.Transition(controller.GetComponent<SoldierController>().SoldierIdleState);
            return;
        }
    }
    public override void Exit(StateMachineController controller)
    {
        //Debug.Log("Exited Patrol State");
    }
}
