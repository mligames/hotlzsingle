﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "State Machine/StateBehaviour/Solider Dead State")]
public class SoldierDeadState : StateBehaviour
{
    public override void Enter(StateMachineController controller)
    {
        Debug.Log("Entered Attack State");
        controller.GetComponent<SoldierController>().Navigator.NavMeshAgent.destination = controller.transform.position;
    }
    public override void StateUpdate(StateMachineController controller)
    {

    }
    public override void Exit(StateMachineController controller)
    {
        //Debug.Log("Exited Attack State");
        controller.GetComponent<SoldierController>().Navigator.NavMeshAgent.isStopped = false;
    }
}
