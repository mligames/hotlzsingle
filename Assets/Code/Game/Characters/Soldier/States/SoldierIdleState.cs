﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "State Machine/StateBehaviour/Solider Idle State")]
public class SoldierIdleState : StateBehaviour
{

    public override void Enter(StateMachineController controller)
    {
        //Debug.Log("Entered Idle State");

    }
    public override void StateUpdate(StateMachineController controller)
    {
        if (controller.GetComponent<SoldierController>().Navigator.HasArrived())
        {
            controller.GetComponent<SoldierController>().Navigator.MoveToRandomDestination();
            controller.Transition(controller.GetComponent<SoldierController>().SoldierPatrolState);
        }


    }
    public override void Exit(StateMachineController controller)
    {
        //Debug.Log("Exited Idle State");
    }
}
