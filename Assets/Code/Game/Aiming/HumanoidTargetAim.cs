﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidTargetAim : MonoBehaviour
{
    private Animator animator;
    private Transform chestTransform;

    public Transform Target;
    public Vector3 Offset;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        chestTransform = animator.GetBoneTransform(HumanBodyBones.Spine);
    }

    // LateUpdate is called after update. THis allows us to apply transform position updates after the animation has played.
    void LateUpdate()
    {
        chestTransform.LookAt(Target.position);
        chestTransform.rotation *= Quaternion.Euler(Offset);
    }

}
