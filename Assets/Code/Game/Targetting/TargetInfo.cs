﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct TargetInfo
{
    public ITarget Target { get; set; }
    public TargetInfo(ITarget targetArg)
    {
        Target = targetArg;
    }
}
