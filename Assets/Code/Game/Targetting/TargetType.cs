﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetType
{
    SOLDIER, TOWER, VEHICLE, STRUCTURE
}
