﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScanner : MonoBehaviour
{
    public FactionData FactionData;
    public float DetectionRadius;
    public List<Target> TargetList;

    [SerializeField] protected Target currentTarget;
    public virtual Target CurrentTarget
    {
        get { return currentTarget; }
        set { currentTarget = value; }
    }

    public LayerMask TargetLayerMask;
    public LayerMask ObsticleLayerMask;

    public TargetAcquired OnTargetAcquired;
    public TargetLost OnTargetLost;

    protected virtual void Awake()
    {
        TargetList = new List<Target>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger)
            return;
        Target target = other.transform.root.GetComponent<Target>();

        if (target == null)
            target = other.GetComponentInParent<Target>();

        if (target == null)
            return;

        if (TargetList.Contains(target))
            return;


        if (IsTargetValid(target))
        {
            if (TargetVisable(target))
            {
                this.AddObserver(OnTargetRemoved, Notifications.TARGET_REMOVED_NOTIFICATION, target);
                TargetList.Add(target);
                GetNearestTarget();
                if (CurrentTarget != null)
                    OnTargetAcquired.Invoke(target);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.isTrigger)
            return;
        Target target = other.transform.root.GetComponent<Target>();

        if (target == null)
            target = other.GetComponentInParent<Target>();

        if (target == null)
            return;

        if (!TargetList.Contains(target))
            return;

        OnTargetRemoved(target, null);
    }
    void OnTargetRemoved(object sender, object e)
    {
        Target target = (Target)sender;
        //Debug.Log(this.transform.root.name + " removed target " + target.TargetPoint.root.gameObject.name);
        this.RemoveObserver(OnTargetRemoved, Notifications.TARGET_REMOVED_NOTIFICATION, sender);
        for (int i = TargetList.Count - 1; i >= 0; i--)
        {
            if (TargetList[i] == target)
            {
                TargetList.RemoveAt(i);
                break;
            }
        }
        if(target == CurrentTarget)
        {
            CurrentTarget = null;
            OnTargetLost.Invoke();
        }
    }
    public bool IsTargetValid(Target targetArg)
    {
        if (targetArg == null || FactionData == null)
            return false;
        if (targetArg.FactionData == null)
            return false;
        return FactionData.CanHarm(targetArg.FactionData);
    }
    public bool TargetVisable(Target targetArg)
    {
        if (targetArg != null)
        {
            Vector3 directionToTargetable = (targetArg.TargetPoint.position - transform.position);
            float distanceToTargetable = Vector3.Distance(transform.position, targetArg.TargetPoint.position);

            if (!Physics.Raycast(transform.position, directionToTargetable, distanceToTargetable, ObsticleLayerMask))
            {
                Debug.DrawRay(transform.position, directionToTargetable, Color.green, 1f, false);
                return true;
            }
        }
        return false;
    }

    public virtual void GetNearestTarget()
    {
        if (TargetList.Count <= 0)
        {
            CurrentTarget = null;
        }

        Target closestTarget = null;

        float closestDistance = DetectionRadius + 1;

        for (int i = TargetList.Count - 1; i >= 0; i--)
        {
            if (TargetList[i] == null)
                continue;

            Target target = TargetList[i].GetComponent<Target>();

            if (target.TargetActive)
            {
                float currentDistance = Vector3.Distance(transform.position, target.TargetPoint.position);
                if (currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    closestTarget = target;
                }
            }
            else
            {
                //OnTargetRemoved(target, null);
                TargetList.RemoveAt(i);
            }
        }
        CurrentTarget = closestTarget;
    }
    protected virtual void ClearTargetList()
    {
        for (int i = TargetList.Count - 1; i >= 0; i--)
        {
            this.RemoveObserver(OnTargetRemoved, Notifications.TARGET_REMOVED_NOTIFICATION, TargetList[i]);
        }
        TargetList.Clear();
    }
    public void ResetTargetter()
    {
        ClearTargetList();
        GetComponent<Collider>().enabled = false;
        GetComponent<Collider>().enabled = true;
    }
#if UNITY_EDITOR

    private void OnValidate()
    {
        //collect the scene name
        if (GetComponent<SphereCollider>() != null)
            GetComponent<SphereCollider>().radius = DetectionRadius;
    }
#endif
}
