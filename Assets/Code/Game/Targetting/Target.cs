﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public FactionData FactionData;
    public Transform TargetPoint;
    public bool TargetActive;

    protected virtual void RemoveTarget()
    {
        TargetActive = false;
        this.PostNotification(Notifications.TARGET_REMOVED_NOTIFICATION);
    }
}
