﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityController : MonoBehaviour
{
    public AttackBehaviour[] availableAttacks;
    public AbilityBehaviour abilityBehaviour;
    public AbilityBehaviour currentAbility;
    public int abilityIndex;

    public LayerMask layerMask;
    private void Start()
    {
        availableAttacks = GetComponentsInChildren<AttackBehaviour>();
        if (availableAttacks.Length > 0)
            currentAbility = availableAttacks[0];

    }
    private void Update()
    {
        AimPoint();
        if (Input.GetMouseButton(0))
            currentAbility.TriggerAbility();

        ChangeAttack(Input.GetAxis("Mouse ScrollWheel"));

    }

    private void AimPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            Vector3 hitPoint = rayHit.point;
            Vector3 targetDir = hitPoint - currentAbility.transform.position;
            //Vector3 newDir = Vector3.RotateTowards(equippedAttack.transform.forward, targetDir, 10f, 0.0f);
            currentAbility.transform.rotation = Quaternion.LookRotation(targetDir);
        }
    }
    private void ChangeAttack(float mouseWheelInput)
    {
        if (mouseWheelInput > 0f)// scroll up
        {
            //Debug.Log(abilitySlot);
            abilityIndex++;
            if (abilityIndex >= availableAttacks.Length)
                abilityIndex = 0;
        }
        else if (mouseWheelInput < 0f) // scroll down
        {
            //Debug.Log(abilitySlot);
            abilityIndex--;
            if (abilityIndex < 0)
                abilityIndex = availableAttacks.Length - 1;
            currentAbility = availableAttacks[abilityIndex];
        }
    }
}
