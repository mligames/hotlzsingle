﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityBehaviour : MonoBehaviour
{
    [SerializeField] protected float abilityTimer;
    public float AbilityCooldownTime;
    public abstract void TriggerAbility();
}
