﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileAttack : AttackBehaviour
{
    public ProjectileFactory ProjectileFactory;

    public override void TriggerAbility()
    {
        if ((Time.time - abilityTimer) < AbilityCooldownTime)
            return;
        ParticleEffect.Play();
        FireProjectile();
        abilityTimer = Time.time;
    }
    public abstract void FireProjectile();

}
