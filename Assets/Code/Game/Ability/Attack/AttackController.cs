﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    public AttackBehaviour[] availableAttacks;
    public AttackBehaviour equippedAttack;
    public int attackIndex;

    public LayerMask layerMask;
    private void Start()
    {
        availableAttacks = GetComponentsInChildren<AttackBehaviour>();
        if (availableAttacks.Length > 0)
            equippedAttack = availableAttacks[0];

    }
    private void Update()
    {
        AimPoint();
        if (Input.GetMouseButton(0))
            equippedAttack.TriggerAbility();

        ChangeAttack(Input.GetAxis("Mouse ScrollWheel"));

    }

    private void AimPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            Vector3 hitPoint = rayHit.point;
            Vector3 targetDir = hitPoint - equippedAttack.transform.position;
            //Vector3 newDir = Vector3.RotateTowards(equippedAttack.transform.forward, targetDir, 10f, 0.0f);
            equippedAttack.transform.rotation = Quaternion.LookRotation(targetDir);
        }
    }
    private void ChangeAttack(float mouseWheelInput)
    {
        if (mouseWheelInput > 0f)// scroll up
        {
            //Debug.Log(abilitySlot);
            attackIndex++;
            if (attackIndex >= availableAttacks.Length)
                attackIndex = 0;
        }
        else if (mouseWheelInput < 0f) // scroll down
        {
            //Debug.Log(abilitySlot);
            attackIndex--;
            if (attackIndex < 0)
                attackIndex = availableAttacks.Length - 1;
            equippedAttack = availableAttacks[attackIndex];
        }
    }

}
