﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearProjectileAttack : ProjectileAttack
{
    public void FireDirectRocket()
    {
        UnityEngine.GameObject rocket = ProjectileFactory.FetchFromPool();
        rocket.transform.SetPositionAndRotation(FirePoint.position, FirePoint.rotation);
        rocket.SetActive(true);
    }
    public override void TriggerAbility()
    {
        base.TriggerAbility();
    }

    public override void FireProjectile()
    {
        UnityEngine.GameObject rocket = ProjectileFactory.FetchFromPool();
        rocket.transform.SetPositionAndRotation(FirePoint.position, FirePoint.rotation);
        rocket.SetActive(true);
    }
}
