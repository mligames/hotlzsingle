﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingProjectileAttack : ProjectileAttack
{
    public float Range;
    public Target LockedUnit;
    public LayerMask AimHitMask;


    public override void TriggerAbility()
    {
        if ((Time.time - abilityTimer) < AbilityCooldownTime)
            return;
        if (LockOnTarget())
        {
            FireProjectile();
            ParticleEffect.Play();
            abilityTimer = Time.time;
        }

    }
    public bool LockOnTarget()
    {
        Ray ray = new Ray
        {
            origin = FirePoint.position,
            direction = FirePoint.forward,
        };

        if (Physics.Raycast(ray, out RaycastHit raycastHit, Range, AimHitMask))
        {
            DamageZone damageable = raycastHit.collider.GetComponent<DamageZone>();
            if (damageable != null)
            {
                LockedUnit = damageable.GetComponentInParent<Target>();
                return true;
            }
            Debug.DrawLine(ray.origin, raycastHit.point, Color.red, 0.5f);
            Debug.Log(transform.root.name + " Hit " + raycastHit.collider.transform.root.name);
        }
        else
        {
            Debug.DrawLine(ray.origin, ray.direction * 20, Color.yellow, 0.5f);
        }
        return false;
    }
    public override void FireProjectile()
    {
        UnityEngine.GameObject rocket = ProjectileFactory.FetchFromPool();
        rocket.transform.SetPositionAndRotation(FirePoint.position, FirePoint.rotation);
        rocket.GetComponent<HomingProjectile>().LockedUnit = LockedUnit;
        rocket.SetActive(true);
    }
}
