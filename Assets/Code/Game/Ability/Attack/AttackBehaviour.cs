﻿using Core.Timers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackBehaviour : AbilityBehaviour
{
    [Header("Inspector Set")]
    public ParticleEffect ParticleEffect;
    public Transform FirePoint;
}
