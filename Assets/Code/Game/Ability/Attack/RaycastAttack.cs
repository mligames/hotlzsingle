﻿using Core.Timers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastAttack : AttackBehaviour
{
    public float Damage;
    public float Range;
    public LayerMask HitMask;

    private void Start()
    {
        abilityTimer = 0;
    }

    private void Update()
    {
        abilityTimer += Time.deltaTime;
    }

    public override void TriggerAbility()
    {
        if (abilityTimer < AbilityCooldownTime)
            return;
        abilityTimer = 0;
        Debug.Log(transform.root.gameObject.name + " Attack");
        Ray ray = new Ray
        {
            origin = FirePoint.position,
            direction = FirePoint.forward,
        };

        if (Physics.Raycast(ray, out RaycastHit raycastHit, Range, HitMask))
        {
            HealthComponent hitObject = raycastHit.collider.transform.root.GetComponent<HealthComponent>();
            //HealthComponent hitObject = raycastHit.collider.GetComponentInParent<HealthComponent>(); // can also do this
            if (hitObject != null)
            {
                Debug.DrawLine(ray.origin, raycastHit.point, Color.red, 0.5f);
                hitObject.Damage(Damage);
            }
        }
        else
        {
            Debug.DrawLine(ray.origin, ray.direction * Range, Color.yellow, 0.5f);
        }

    }
}
