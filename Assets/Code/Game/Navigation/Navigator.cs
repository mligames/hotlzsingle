﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Navigator : MonoBehaviour
{
    public NavMeshAgent NavMeshAgent;
    public float areaWonderRadius;
    // Start is called before the first frame update
    void Start()
    {
        if(NavMeshAgent == null)
            NavMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MoveToRandomDestination()
    {
        NavMeshAgent.destination = GetRandomPointInArea();
    }
    private Vector3 GetRandomPointInArea()
    {

        return new Vector3(0 + Random.Range(-areaWonderRadius, areaWonderRadius), 0f, 0 + Random.Range(-areaWonderRadius, areaWonderRadius));
        //return new Vector3(transform.position.x + Random.Range(-areaWonderRadius, areaWonderRadius), 0f, transform.position.z + Random.Range(-areaWonderRadius, areaWonderRadius));
    }
    public bool HasArrived()
    {
        if (NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance)
            return true;
        else
            return false;
    }
}
