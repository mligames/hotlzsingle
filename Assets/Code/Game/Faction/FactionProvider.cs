﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionProvider : Singleton<FactionProvider>
{
    public FactionData NoneFactionData;
    public FactionData BlueFactionData;
    public FactionData RedFactionData;
    public FactionData NeutralFactionData;
}
