﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionFactory : MonoBehaviour
{
    public FactionManager FactionManager;
    public SoldierFactory SoldierFactory;

    // Start is called before the first frame update
    void Start()
    {
        SoldierFactory = GetComponentInChildren<SoldierFactory>();
        SoldierFactory.factoryProductionStatus = GameEnum.FactoryProductionStatus.ON;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TurnOnSoldierProduction()
    {
        SoldierFactory.factoryProductionStatus = GameEnum.FactoryProductionStatus.ON;
    }
    public void TurnOffSoldierProduction()
    {
        SoldierFactory.factoryProductionStatus = GameEnum.FactoryProductionStatus.OFF;
    }
}
