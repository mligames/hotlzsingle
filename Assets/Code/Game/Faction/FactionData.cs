﻿using GameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newFactionData", menuName = "Faction/Alignment")]
public class FactionData : ScriptableObject
{
    public string FactionName;
    public FactionType FactionType;
    public Material factionColorMaterial;
    /// <summary>
    /// A collection of other alignment objects that we can harm
    /// </summary>
    public List<FactionData> enemies;

    public bool CanHarm(FactionData other)
    {
        if (other == null)
        {
            return true;
        }
        return other != null && enemies.Contains(other);
    }
    public void ChangeFactionColor(MeshRenderer meshArg)
    {
        meshArg.material = factionColorMaterial;
    }
}
