﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterMovement : MonoBehaviour
{
    public Rigidbody physicisBody;
    public KeyCodeVariable upKey;
    public KeyCodeVariable downKey;
    public float rotationSpeed;
    public float movementSpeed;
    public LayerMask layerMask;
    // Start is called before the first frame update
    public void MouseRotation()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(camRay, out RaycastHit floorHit, 500f, layerMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(playerToMouse);
            lookRotation.x = 0f;
            lookRotation.z = 0f;
            physicisBody.MoveRotation(Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime));
        }
    }
    public void HorizontalFlight()
    {
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        moveDirection = moveDirection.normalized * movementSpeed * 50f * Time.deltaTime;
        physicisBody.AddForce(moveDirection);

        if (upKey.KeyPressValue())
            physicisBody.AddForce(transform.up * movementSpeed);
        if (downKey.KeyPressValue())
            physicisBody.AddForce(-transform.up + new Vector3(0,0,1) * movementSpeed);
    }
    public void VerticalFlight()
    {
        if (upKey.KeyPressValue())
            physicisBody.AddForce(transform.up * movementSpeed);
        if (downKey.KeyPressValue())
            physicisBody.AddForce(-transform.up * movementSpeed);
    }

    public void AutoLevel() {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, transform.rotation.y, 0), Time.time * 1f);
    }
}
