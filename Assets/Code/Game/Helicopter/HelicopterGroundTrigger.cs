﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterGroundTrigger : MonoBehaviour
{
    HelicopterController controller;
    public Collider[] ignoreColliders;
    public LayerMask groundMask;
    // Start is called before the first frame update
    void Start()
    {
        controller = transform.root.GetComponent<HelicopterController>();
        for (int i = 0; i < ignoreColliders.Length; i++)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), ignoreColliders[i].GetComponent<Collider>(), true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Terrain"))
        {
            controller.grounded = true;
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Terrain"))
        {
            controller.grounded = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Terrain"))
        {
            controller.grounded = false;
        }
    }
}
