﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HelicopterAbilityController : MonoBehaviour
{
    public HelicopterController controller;
    public AttackBehaviour[] availableAttacks;
    public AttackBehaviour currentAttack;
    //public HelicopterCallSoliderAbility callSoliderAbility;
    //public HelicopterDeploySoliderAbility deploySoliderAbility;
    public int abilityIndex;
    public LayerMask layerMask;
    private void Start()
    {
        InitComponent();
    }
    private void Update()
    {
        if (!controller.grounded)
        {
            AimPoint();
            if (Input.GetMouseButton(0))
                currentAttack.TriggerAbility();
        }
        //else
        //{
        //    if (callSoliderAbility.CallSoliderInput)
        //    {
        //        callSoliderAbility.TriggerAbility();
        //    }
        //    if (deploySoliderAbility.DeploySoliderInput)
        //    {
        //        deploySoliderAbility.TriggerAbility();
        //    }
            
        //}

        ChangeAttack(Input.GetAxis("Mouse ScrollWheel"));
        //CooldownAttacks();
    }

    private void AimPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            Vector3 hitPoint = rayHit.point;
            Vector3 targetDir = hitPoint - currentAttack.transform.position;
            //Vector3 newDir = Vector3.RotateTowards(equippedAttack.transform.forward, targetDir, 10f, 0.0f);
            currentAttack.transform.rotation = Quaternion.LookRotation(targetDir);
        }
    }
    //private void CooldownAttacks()
    //{
    //    for (int i = 0; i < availableAttacks.Length; i++)
    //    {
    //        availableAttacks[i].cooldownTimer.Tick(Time.deltaTime);
    //    }
    //}
    private void ChangeAttack(float mouseWheelInput)
    {
        if (mouseWheelInput > 0f)// scroll up
        {
            //Debug.Log(abilitySlot);
            abilityIndex++;
            if (abilityIndex >= availableAttacks.Length)
                abilityIndex = 0;
        }
        else if (mouseWheelInput < 0f) // scroll down
        {
            //Debug.Log(abilitySlot);
            abilityIndex--;
            if (abilityIndex < 0)
                abilityIndex = availableAttacks.Length - 1;
            currentAttack = availableAttacks[abilityIndex];
        }
    }
    #region Init
    public void InitComponent()
    {
        if(controller == null)
            controller = transform.root.GetComponent<HelicopterController>();
        availableAttacks = GetComponentsInChildren<AttackBehaviour>();
        if (availableAttacks.Length > 0)
            currentAttack = availableAttacks[0];
    }
    #endregion
}
