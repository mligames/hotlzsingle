﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterController : Target
{
    public Rigidbody physicsBody;
    public HelicopterMovement movement;
    public bool grounded;
    // Start is called before the first frame update
    private void Start()
    {
        if (physicsBody == null)
            physicsBody = GetComponent<Rigidbody>();
        physicsBody.centerOfMass = transform.position;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (grounded)
        {
            movement.VerticalFlight();
            return;
        }
        movement.HorizontalFlight();
        movement.VerticalFlight();
        movement.MouseRotation();
    }

}
