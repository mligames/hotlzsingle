﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierSpawn : MonoBehaviour
{
    public float spawnFrequency;
    public UnityEngine.GameObject SoldierPrefab;
    public Transform SpawnLocation;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnSoldierInstance", spawnFrequency, spawnFrequency);
    }

    private void SpawnSoldierInstance()
    {
        UnityEngine.GameObject soldier = Instantiate(SoldierPrefab);
        soldier.transform.position = SpawnLocation.position;
    }
}
