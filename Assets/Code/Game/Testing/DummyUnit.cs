﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyUnit : Target
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {
            HandlePickUp();
        }
        if (Input.GetKey(KeyCode.Z))
        {
            HandleDropOff();
        }
    }
    public void HandlePickUp()
    {
        gameObject.SetActive(false);
    }
    public void HandleDropOff()
    {
        gameObject.SetActive(true);
    }
    public void HandleDeath()
    {
        Debug.Log("Died");
        gameObject.SetActive(false);
    }

}
