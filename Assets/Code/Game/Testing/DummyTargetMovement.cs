﻿using Core.Timers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyTargetMovement : MonoBehaviour
{
    public float speed;
    //public RepeatingTimer changeDirectionTimer;
    public bool left;
    // Start is called before the first frame update
    void Start()
    {
        //if (changeDirectionTimer == null)
        //    changeDirectionTimer = GetComponent<RepeatingTimer>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (changeDirectionTimer.normalizedProgress <= 0)
        //{
        //    Debug.Log("ChangeDirectionTimer Donne");
        //    left = left == true ? false : true;
        //}


        Vector3 direction = Vector3.zero;
        if (left)
            direction = -Vector3.right;
        else
            direction = Vector3.right;
        transform.Translate(direction * Time.deltaTime * speed);
    }
}
