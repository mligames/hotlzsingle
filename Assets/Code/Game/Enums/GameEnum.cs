﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameEnum
{
    public enum FactionType {NONE, BLUE, RED, NEUTRAL }
    public enum CheckPointSelectMode { FIXED, DISTRIBUTED, RANDOM }
    public enum DestinationType { GARRISON, CHECK_POINT_AREA, TARGET }
    public enum RotateAxis { X, Y, Z }
    public enum FactoryProductionStatus { ON, OFF}
}
