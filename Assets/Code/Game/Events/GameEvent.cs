﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvent
{
    #region Scene Events
    [Serializable] public class FadeComplete : UnityEvent<bool> { }
    [Serializable] public class SceneChangeComplete : UnityEvent<bool> { }
    #endregion

    [Serializable] public class RemoveTargetEvent : UnityEvent<Target> { }
    [Serializable] public class UnitDeath : UnityEvent { }
    [Serializable] public class TimerFinished : UnityEvent { }
    #region Targetting Events
    [Serializable] public class TargetAcquired : UnityEvent<Target> { }
    [Serializable] public class TargetLost : UnityEvent { }
    #endregion
    #region Garrison Events
    [Serializable] public class GarrisonFull : UnityEvent<bool> { }
    [Serializable] public class GarrisonTaken : UnityEvent<FactionData> { }
    //[Serializable] public class GarrisonExitedRange : UnityEvent<GarrisonBehaviour> { }
    //[Serializable] public class GarrisonLost : UnityEvent { }
    #endregion
    #region Turret Events
    [Serializable] public class TurretDestroyed : UnityEvent { }
    //[Serializable] public class GarrisonExitedRange : UnityEvent<GarrisonBehaviour> { }
    //[Serializable] public class GarrisonLost : UnityEvent { }
    #endregion
    #region TerritoryEvents
    [Serializable] public class FactionOwnerUpdate : UnityEvent<FactionData> { }
    #endregion
    #region MunitionEvents
    [Serializable] public class MunitionDetonation : UnityEvent { }
    #endregion
}

