﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    [SerializeField] private float maxHP;
    public float MaxHP { get => maxHP; set => maxHP = value; }

    [SerializeField] private float currentHP;
    public float CurrentHP { get => currentHP; set => currentHP = value; }

    [SerializeField] private bool dead;
    public bool Dead { get => dead; set => dead = value; }

    public event Action<Damageable> Died;
    public event Action<Damageable> Deactivated;
    protected virtual void Awake()
    {
        currentHP = maxHP;
    }
    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }

    public void Damage(float amount)
    {
        currentHP -= amount;
        if (currentHP <= 0)
        {
            Died?.Invoke(this);
            dead = true;
        }
    }

    public void Heal(float amount)
    {
        currentHP += amount;
        if (currentHP >= maxHP)
            currentHP = maxHP;
    }
    public virtual void Kill()
    {
        Damage(maxHP);
    }
    public void ResetHealth()
    {
        currentHP = maxHP;
        dead = false;
    }
    /// <summary>
    /// Deactivates this damageable without killing it
    /// </summary>
    public virtual void Deactivate()
    {
        // Set health to zero so that this behaviour appears to be dead. This will not fire death events
        OnDeactivated();
    }

    /// <summary>
    /// Fires the Deactivated event
    /// </summary>
    private void OnDeactivated()
    {
        Deactivated?.Invoke(this);
    }
}
