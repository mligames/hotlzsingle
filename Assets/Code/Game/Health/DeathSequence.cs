﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSequence : MonoBehaviour
{
    public float corpseLingerTime;
    public DamageZone[] damageZones;
    public List<Collider> physicsColliderList;
    // Start is called before the first frame update
    void Start()
    {
        physicsColliderList = new List<Collider>();
        damageZones = GetComponentsInChildren<DamageZone>();
        for (int i = 0; i < damageZones.Length; i++)
        {
            Collider coldr = damageZones[i].GetComponent<Collider>();
            if (coldr != null)
                physicsColliderList.Add(coldr);
        }
    }
    public void PlaySequence()
    {
        StopCoroutine(DecaySequence());
        StartCoroutine(DecaySequence());
    }
    public void ActivateAllDamageColliders()
    {
        for (int i = physicsColliderList.Count - 1; i >= 0; i--)
        {
            physicsColliderList[i].enabled = true;
        }
    }
    public void DeactivateAllDamageColliders()
    {
        for (int i = physicsColliderList.Count - 1; i >= 0; i--)
        {
            physicsColliderList[i].enabled = false;
        }
    }
    public void DeathDecay()
    {
        transform.Translate(-Vector3.up * .5f * Time.deltaTime);
    }
    IEnumerator DecaySequence()
    {
        DeactivateAllDamageColliders();
        InvokeRepeating("DeathDecay", corpseLingerTime, 0.01f);
        yield return new WaitForSeconds(corpseLingerTime + 2.5f);
        CancelInvoke();
        gameObject.SetActive(false);
    }

    #region Resetting
    public void ResetComponent()
    {
        ActivateAllDamageColliders();
    }
    #endregion
}
