﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
    public bool dead;
    public UnitDeath OnUnitDeath;

    private void Start()
    {
        ResetComponent();
    }
    public void Damage(float damageAmount)
    {
        if (dead)
            return;
        currentHealth -= damageAmount;
        if (currentHealth <= 0)
        {
            dead = true;
            OnUnitDeath.Invoke();
        }

    }
    #region Resetting
    public void ResetComponent()
    {
        currentHealth = maxHealth;
        dead = false;
    }
    #endregion
}
