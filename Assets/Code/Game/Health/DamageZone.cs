﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
    public HealthComponent HealthComponent;
    // Start is called before the first frame update
    void Start()
    {
        if (HealthComponent == null)
            HealthComponent = transform.root.GetComponent<HealthComponent>();
    }

    public void ApplyDamage(float weaponDamage)
    {
        if (HealthComponent != null)
            HealthComponent.Damage(weaponDamage);
    }
}
